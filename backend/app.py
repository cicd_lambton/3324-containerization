from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/')
def get_data():
    data = {'message': 'Hello from sleeping beauties!This is our Backend app'}
    return jsonify(data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
